﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.SurveyApp.WebAPI.Controllers
{
    [RoutePrefix("api/survey")]
    public class SurveyQuestionsController : ApiController
    {
        private readonly ISurveyQuestionsService _surveyQuestionsService;

        public SurveyQuestionsController(ISurveyQuestionsService surveyQuestionsService)
        {
            _surveyQuestionsService = surveyQuestionsService;
        }

        [Route("questions/{surveyId:int}/{questionId:int}")]
        [HttpPost]
        public IHttpActionResult AddQuestionToSurvey([FromUri] int surveyId, [FromUri] int questionId)
        {
            if (surveyId <= 0 || questionId <= 0) return BadRequest("Bad format request");
            bool correctlyadded = _surveyQuestionsService.AddQuestionToSurvey(questionId, surveyId);
            if (!correctlyadded) return BadRequest("unable to add question");
            return Ok();
        }

        [Route("questions/{id:int}")]
        [HttpGet]
        public IHttpActionResult GetQuestionsBySurvey([FromUri] int id)
        {
            if (id <= 0) return BadRequest("Bad format request");
            ICollection<Question> questions = _surveyQuestionsService.GetQuestionsBySurvey(id);
            if (questions == null) return NotFound();
            ICollection<QuestionDTO> questionsDTO = questions.Select(q => new QuestionDTO
            {
                Id = q.Id,
                CreatedDate = q.CreatedDate,
                IsActive = q.IsActive,
                ModifiedDate = q.ModifiedDate,
                QuestionTypeId = q.QuestionTypeId,
                Text = q.Text,
            }).ToList();

            return Ok(questionsDTO);
        }
    }
}
