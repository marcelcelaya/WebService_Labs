﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.WebAPI.Models;
using System.Web.Http;

namespace Softtek.Academy2018.SurveyApp.WebAPI.Controllers
{
    [RoutePrefix("api/survey")]
    public class SurveyController : ApiController
    {
        private readonly ISurveyService _surveyService;

        public SurveyController(ISurveyService surveyService)
        {
            _surveyService = surveyService;
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Create([FromBody] SurveyDTO surveyDTO)
        {
            if (surveyDTO == null) return BadRequest("Request is null");

            Survey survey = new Survey
            {
                Description=surveyDTO.Description,
                Title=surveyDTO.Title,
                CreatedDate=surveyDTO.CreatedDate,
                ModifiedDate=surveyDTO.ModifiedDate,
                IsArchived=surveyDTO.IsArchived
            };

            int id = _surveyService.Add(survey);

            if (id <= 0) return BadRequest("Unable to create survey");

            var payload = new { SurveyId = id };

            return Ok(payload);
        }

        [Route("{id:int}")]
        [HttpGet]
        public IHttpActionResult Get([FromUri] int id)
        {
            Survey survey = _surveyService.Get(id);

            if (survey == null) return NotFound();

            SurveyDTO surveyDTO = new SurveyDTO
            {
                Id=survey.Id,
                Title=survey.Title,
                Description=survey.Description,
                CreatedDate=survey.CreatedDate,
                IsArchived=survey.IsArchived,
                ModifiedDate=survey.ModifiedDate
            };
            return Ok(surveyDTO);
        }

        [Route("{id:int}")]
        [HttpDelete]
        public IHttpActionResult Delete([FromUri] int id)
        {
            var result = _surveyService.Delete(id);

            if (!result) return BadRequest("Unable to delete survey");

            return Ok();
        }

        [Route("{id:int}")]
        [HttpPut]
        public IHttpActionResult Update([FromUri] int id, [FromBody] SurveyDTO surveyDTO)
        {
            if (surveyDTO == null) return BadRequest("User is null");
            Survey survey = new Survey
            {
                Id = surveyDTO.Id,
                Title = surveyDTO.Title,
                Description = surveyDTO.Description,
                CreatedDate = surveyDTO.CreatedDate,
                IsArchived = surveyDTO.IsArchived,
                ModifiedDate = surveyDTO.ModifiedDate,
            };

            bool result = _surveyService.Update(survey);

            if (!result) return BadRequest("Unable to update survey");
            return Ok();
        }
    }
}
