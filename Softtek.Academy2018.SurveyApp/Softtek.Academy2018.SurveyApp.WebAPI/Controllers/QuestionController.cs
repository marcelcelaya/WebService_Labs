﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.QuestionApp.WebAPI.Controllers
{
    [RoutePrefix("api/question")]
    public class QuestionController : ApiController
    {
        private readonly IQuestionService _questionService;

        public QuestionController(IQuestionService questionService)
        {
            _questionService = questionService;
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Create([FromBody] QuestionDTO questionDTO)
        {
            if (questionDTO == null) return BadRequest("Request is null");

            Question question = new Question
            {
                Text = questionDTO.Text,
                IsActive = questionDTO.IsActive,
                QuestionTypeId = questionDTO.QuestionTypeId
            };

            int id = _questionService.Add(question);

            if (id <= 0) return BadRequest("Unable to create question");

            var payload = new { QuestionId = id };

            return Ok(payload);
        }

        [Route("{id:int}")]
        [HttpGet]
        public IHttpActionResult Get([FromUri] int id)
        {
            Question question = _questionService.Get(id);

            if (question == null) return NotFound();

            QuestionDTO QuestionDTO = new QuestionDTO
            {
                Id = question.Id,
                CreatedDate = question.CreatedDate,
                IsActive = question.IsActive,
                ModifiedDate = question.ModifiedDate,
                QuestionTypeId = question.QuestionTypeId,
                Text=question.Text,
            };
            return Ok(QuestionDTO);
        }

        [Route("{id:int}")]
        [HttpDelete]
        public IHttpActionResult Delete([FromUri] int id)
        {
            var result = _questionService.Delete(id);

            if (!result) return BadRequest("Unable to delete Question");

            return Ok();
        }

        [Route("{id:int}")]
        [HttpPut]
        public IHttpActionResult Update([FromUri] int id, [FromBody] QuestionDTO questionDTO)
        {
            if (questionDTO == null) return BadRequest("User is null");
            Question question = new Question
            {
                Id = questionDTO.Id,
                CreatedDate = questionDTO.CreatedDate,
                IsActive = questionDTO.IsActive,
                ModifiedDate = questionDTO.ModifiedDate,
                QuestionTypeId = questionDTO.QuestionTypeId,
                Text = questionDTO.Text,
            };

            bool result = _questionService.Update(question);

            if (!result) return BadRequest("Unable to update question");
            return Ok();
        }

    }
}
