﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.SurveyApp.WebAPI.Controllers
{
    [RoutePrefix("api/question")]
    public class QuestionOptionsController : ApiController
    {
        private readonly IQuestionOptionsService _questionOptionService;

        public QuestionOptionsController(IQuestionOptionsService questionOptionService)
        {
            _questionOptionService = questionOptionService;
        }

        [Route("options/{questionId:int}/{optionId:int}")]
        [HttpPost]
        public IHttpActionResult AddOptionToQuestion([FromUri] int questionId, [FromUri] int optionId)
        {
            if (questionId <= 0 || optionId <= 0) return BadRequest("Bad format request");
            bool correctlyadded = _questionOptionService.AddOptionToQuestion(questionId, optionId);
            if (!correctlyadded) return BadRequest("unable to add option");
            return Ok();
        }
        [Route("options/{id:int}")]
        [HttpGet]
        public IHttpActionResult GetOptionsByQuestion([FromUri] int id)
        {
            if (id <= 0) return BadRequest("Bad format request");
            ICollection<Option> options = _questionOptionService.GetOptionsByQuestion(id);
            
            if (options == null) return NotFound();
            ICollection<OptionDTO> optionsDTO = options.Select(o => new OptionDTO
            {
                Id = o.Id,
                CreatedDate = o.CreatedDate,
                ModifiedDate = o.ModifiedDate,
                Text = o.Text,
            }).ToList();
            return Ok(optionsDTO);
        }
    }
}
