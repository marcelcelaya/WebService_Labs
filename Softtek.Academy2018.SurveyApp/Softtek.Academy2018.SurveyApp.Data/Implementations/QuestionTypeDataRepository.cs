﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class QuestionTypeDataRepository : IQuestionTypeRepository
    {
        public int Add(QuestionType entity)
        {
            if (entity == null) return 0;
            using (var ctx = new SuerveyDbContext())
            {
                entity.CreatedDate = DateTime.Now;
                entity.ModifiedDate = null;
                ctx.QuestionTypes.Add(entity);
                ctx.SaveChanges();
                return entity.Id;
            }
        }

        public bool Delete(int id)
        {
            using (var ctx = new SuerveyDbContext())
            {
                QuestionType currentQuestionType = ctx.QuestionTypes.SingleOrDefault(x => x.Id == id);
                if (currentQuestionType == null) return false;
                ctx.QuestionTypes.Remove(currentQuestionType);
                ctx.SaveChanges();
                return true;
            }
        }

        public QuestionType Get(int id)
        {
            using (var ctx = new SuerveyDbContext())
            {
                return ctx.QuestionTypes.AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        public ICollection<QuestionType> GetAll()
        {
            using (var ctx = new SuerveyDbContext())
            {
                return ctx.QuestionTypes.ToList();
            }
        }

        public bool Update(QuestionType entity)
        {
            using (var ctx = new SuerveyDbContext())
            {
                QuestionType currentQuestionType = ctx.QuestionTypes.SingleOrDefault(x => x.Id == entity.Id);
                if (currentQuestionType == null) return false;
                currentQuestionType.Description = entity.Description;
                currentQuestionType.ModifiedDate = DateTime.Now;
                ctx.SaveChanges();
                return true;
            }
        }
    }
}
