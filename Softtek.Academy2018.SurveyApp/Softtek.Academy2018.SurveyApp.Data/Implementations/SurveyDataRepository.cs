﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class SurveyDataRepository : ISurveyRepository
    {
        public int Add(Survey entity)
        {
            if (entity == null) return 0;
            using (var ctx = new SuerveyDbContext())
            {
                entity.CreatedDate = DateTime.Now;
                entity.ModifiedDate = null;
                ctx.Surveys.Add(entity);
                ctx.SaveChanges();
                return entity.Id;
            }
        }

        public bool AddQuestion(int surveyId, int questionId)
        {
            using (var ctx = new SuerveyDbContext())
            {
                Survey currentSurvey = ctx.Surveys.SingleOrDefault(x => x.Id == surveyId);
                if (currentSurvey == null) return false;
                Question currentQuestion = ctx.Questions.SingleOrDefault(x => x.Id == questionId);
                if (currentQuestion == null) return false;
                currentSurvey.Questions.Add(currentQuestion);
                return true;
            }
        }

        public bool ContainsQuestion(int surveyId, int questionId)
        {
            using (var ctx = new SuerveyDbContext())
            {
                return ctx.Surveys.Any(x => x.Id == surveyId && x.Questions.Any(q => q.Id == questionId));
            }
        }

        public bool Delete(int id)
        {
            using (var ctx = new SuerveyDbContext())
            {
                Survey currentSurvey = ctx.Surveys.SingleOrDefault(x => x.Id == id);
                if (currentSurvey == null) return false;
                currentSurvey.IsArchived = true;
                ctx.SaveChanges();
                return true;
            }
        }

        public bool Exist(int surveyId)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Surveys.Any(x => x.Id == surveyId);
            }
        }

        public Survey Get(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Surveys.SingleOrDefault(x => x.Id == id && !x.IsArchived);
            }
        }

        public ICollection<Question> GetQuestionsBySurvey(int surveyId)
        {
            using (var ctx = new SuerveyDbContext())
            {
                ICollection<Question> questions = ctx.Surveys.Where(x => x.Id == surveyId).SelectMany(q=>q.Questions).ToList();
                if (questions == null || questions.Count <= 0) return null;
                return questions;
            }
        }

        public bool Update(Survey entity)
        {
            using (var ctx = new SuerveyDbContext())
            {
                Survey currentSurvey = ctx.Surveys.SingleOrDefault(x => x.Id == entity.Id);
                if (currentSurvey == null) return false;
                currentSurvey.Title = entity.Title;
                currentSurvey.Description = entity.Description;
                currentSurvey.ModifiedDate = DateTime.Now;
                ctx.SaveChanges();
                return true;
            }
        }
    }
}
