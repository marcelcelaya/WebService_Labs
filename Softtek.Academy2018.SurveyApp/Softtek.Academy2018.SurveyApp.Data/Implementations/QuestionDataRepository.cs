﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class QuestionDataRepository : IQuestionRepository
    {
        public int Add(Question entity)
        {
            if (entity == null) return 0;
            using (var ctx = new SuerveyDbContext())
            {
                entity.CreatedDate = DateTime.Now;
                entity.ModifiedDate = null;
                ctx.Questions.Add(entity);
                ctx.SaveChanges();
                return entity.Id;
            }
        }

        public bool AddOption(int questionId, int optionId)
        {
            using (var ctx = new SuerveyDbContext())
            {
                Question currentQuestion = ctx.Questions.SingleOrDefault(x => x.Id == questionId);
                if (currentQuestion == null) return false;
                Option currentOption = ctx.Options.SingleOrDefault(x => x.Id == optionId);
                if (currentOption == null) return false;
                currentQuestion.Options.Add(currentOption);
                ctx.SaveChanges();
                return true;
            }
        }

        public bool ContainsOption(int questionId, int optionId)
        {
            using (var ctx = new SuerveyDbContext())
            {
                return ctx.Questions.Any(x => x.Id == questionId && x.Options.Any(o => o.Id == optionId));
            }
        }
        public bool Delete(int id)
        {
            using (var ctx = new SuerveyDbContext())
            {
                Question currentQuestion = ctx.Questions.SingleOrDefault(x => x.Id == id);
                if (currentQuestion == null) return false;
                currentQuestion.IsActive = false;
                ctx.SaveChanges();
                return true;
            }
        }

        public bool Exist(int questionId)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Questions.Any(x => x.Id == questionId);
            }
        }

        public Question Get(int id)
        {
            using (var ctx = new SuerveyDbContext())
            {
                return ctx.Questions.AsNoTracking().SingleOrDefault(x => x.Id == id && x.IsActive);
            }
        }

        public ICollection<Option> GetOptionsByQuestion(int questionId)
        {
            using (var ctx = new SuerveyDbContext())
            {
                ICollection options= ctx.Questions.Where(x => x.Id == questionId).SelectMany(a => a.Options).ToList();
                if (options==null || options.Count <= 0) return null;
                return ctx.Questions.Where(x => x.Id == questionId).SelectMany(a => a.Options).ToList();
            }
        }

        public bool Update(Question entity)
        {
            using (var ctx = new SuerveyDbContext())
            {
                Question currentQuestion = ctx.Questions.SingleOrDefault(x => x.Id == entity.Id);
                if (currentQuestion == null) return false;
                currentQuestion.ModifiedDate = DateTime.Now;
                currentQuestion.Text = entity.Text;
                currentQuestion.ModifiedDate = DateTime.Now;
                ctx.SaveChanges();
                return true;
            }
        }
    }
}
