﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class OptionDataRepository : IOptionRepository
    {
        public int Add(Option entity)
        {
            if (entity == null) return 0;
            using (var ctx = new SuerveyDbContext())
            {
                entity.CreatedDate = DateTime.Now;
                entity.ModifiedDate = null;
                ctx.Options.Add(entity);
                ctx.SaveChanges();
                return entity.Id;
            }
        }

        public bool Delete(int id)
        {
            using (var ctx = new SuerveyDbContext())
            {
                Option currentOption = ctx.Options.SingleOrDefault(x => x.Id == id);
                if (currentOption == null) return false;
                ctx.Options.Remove(currentOption);
                ctx.SaveChanges();
                return true;
            }
        }

        public bool Exist(int optionId)
        {
            using (var ctx = new SuerveyDbContext())
            {
                return ctx.Options.Any(x => x.Id == optionId);
            }
        }

        public Option Get(int id)
        {
            using (var ctx = new SuerveyDbContext())
            {
                return ctx.Options.AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        public bool Update(Option entity)
        {
            using (var ctx = new SuerveyDbContext())
            {
                Option currentOption = ctx.Options.SingleOrDefault(x => x.Id == entity.Id);
                if (currentOption == null) return false;
                currentOption.Text = entity.Text;
                currentOption.ModifiedDate = DateTime.Now;
                ctx.SaveChanges();
                return true;
            }
        }
    }
}
