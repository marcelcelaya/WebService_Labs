﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Collections.Generic;

namespace Softtek.Academy2018.SurveyApp.Data.Contracts
{
    public interface IQuestionRepository : IGenericRepository<Question>
    {
        bool Exist(int questionId);
        bool ContainsOption(int questionId, int optionId);
        ICollection<Option> GetOptionsByQuestion(int questionId);
        bool AddOption(int questionId, int optionId);
    }
}
