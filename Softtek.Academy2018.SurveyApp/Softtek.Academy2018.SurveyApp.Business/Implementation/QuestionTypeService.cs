﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Implementation
{
    public class QuestionTypeService : IQuestionTypeService
    {
        private readonly IQuestionTypeRepository _questionTypeRepository;

        public QuestionTypeService(IQuestionTypeRepository questionTypeRepository)
        {
            _questionTypeRepository = questionTypeRepository;
        }

        public int Add(QuestionType questionType)
        {
            if (questionType == null) return 0;
            if (string.IsNullOrEmpty(questionType.Description)) return 0;
            return (_questionTypeRepository.Add(questionType));
        }

        public bool Delete(int id)
        {
            if (id <= 0) return false;
            QuestionType questionType = _questionTypeRepository.Get(id);
            if (questionType == null) return false;
            return (_questionTypeRepository.Delete(questionType.Id));
        }

        public QuestionType Get(int id)
        {
            if (id <= 0) return null;
            QuestionType questionType = _questionTypeRepository.Get(id);
            if (questionType == null) return null;
            return _questionTypeRepository.Get(id);
        }

        public bool Update(QuestionType questionType)
        {
            if (questionType.Id <= 0) return false;
            if (string.IsNullOrEmpty(questionType.Description)) return false;
            return _questionTypeRepository.Update(questionType);
        }
    }
}
