﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Implementation
{

    public class OptionService : IOptionService
    {
        private readonly IOptionRepository _optionRepository;
        public OptionService(IOptionRepository optionRepository)
        {
            _optionRepository = optionRepository;
        }

        public int Add(Option option)
        {
            if (string.IsNullOrEmpty(option.Text))
                return 0;
            return _optionRepository.Add(option);
        }

        public bool Delete(int id)
        {
            if (id < 0) return false;
            Option option = _optionRepository.Get(id);
            if (option == null) return false;
            return _optionRepository.Delete(id);
        }
        public Option Get(int id)
        {
            if (id < 0) return null;
            Option option = _optionRepository.Get(id);
            return _optionRepository.Get(id);
        }

        public bool Update(Option option)
        {
            if (option.Id <= 0) return false;
            if (string.IsNullOrEmpty(option.Text))
                return false;
            _optionRepository.Update(option);
            return true;
        }
    }
}
