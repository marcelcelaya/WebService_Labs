﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Implementation
{
    public class QuestionOptionService : IQuestionOptionsService
    {
        private readonly IQuestionRepository _questionRepository;
        private readonly IOptionRepository _optionRepository;

        public QuestionOptionService(IQuestionRepository questionRepository, IOptionRepository optionRepository)
        {
            _questionRepository = questionRepository;
            _optionRepository = optionRepository;
        }

        public bool AddOptionToQuestion(int questionId, int optionId)
        {
            if (questionId <= 0 || optionId <= 0) return false;
            bool questionExist = _questionRepository.Exist(questionId);
            if (!questionExist) return false;
            bool optionExist = _optionRepository.Exist(optionId);
            if (!optionExist) return false;
            bool questionContainOption = _questionRepository.ContainsOption(questionId, optionId);
            if (questionContainOption) return false;
            return _questionRepository.AddOption(questionId, optionId);
        }

        public ICollection<Option> GetOptionsByQuestion(int questionId)
        {
            if (questionId <= 0) return null;
            return _questionRepository.GetOptionsByQuestion(questionId);
        }
    }
}
