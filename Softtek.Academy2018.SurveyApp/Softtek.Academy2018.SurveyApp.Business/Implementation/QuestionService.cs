﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Implementation
{
    public class QuestionService : IQuestionService
    {
        private readonly IQuestionRepository _questionRepository;
        public QuestionService(IQuestionRepository questionRepository)
        {
            _questionRepository = questionRepository;
        }
        public int Add(Question question)
        {
            if (string.IsNullOrEmpty(question.Text)) return 0;
            return _questionRepository.Add(question);
        }

        public bool Delete(int id)
        {
            if (id <= 0) return false;
            Question question = _questionRepository.Get(id);
            if (question == null) return false;
            return _questionRepository.Delete(question.Id);
            
        }

        public Question Get(int id)
        {
            if (id < 0) return null;
            Question question = _questionRepository.Get(id);
            if (question == null) return null;
            return _questionRepository.Get(id);
        }

        public bool Update(Question question)
        {
            if (question.Id <= 0) return false;
            if (string.IsNullOrEmpty(question.Text)) return false;
            if (question.QuestionTypeId <= 0) return false;
            return _questionRepository.Update(question);
        }
    }
}
