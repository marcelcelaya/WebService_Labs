﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Implementation
{
    public class SurveyService : ISurveyService
    {
        private readonly ISurveyRepository _surveyRepository;

        public SurveyService(ISurveyRepository surveyRepository)
        {
            _surveyRepository = surveyRepository;
        }

        public int Add(Survey survey)
        {
            if (survey == null) return 0;
            if (string.IsNullOrEmpty(survey.Description) ||
                string.IsNullOrEmpty(survey.Title))
                return 0;
            return _surveyRepository.Add(survey);
        }

        public bool Delete(int id)
        {
            if (id <= 0) return false;
            Survey survey = _surveyRepository.Get(id);
            if (survey == null) return false;
            return (_surveyRepository.Delete(id));
        }

        public Survey Get(int id)
        {
            if (id <= 0) return null;
            return _surveyRepository.Get(id);

        }

        public bool Update(Survey survey)
        {
            if (survey.Id <= 0) return false;
            if (string.IsNullOrEmpty(survey.Title)) return false;
            return (_surveyRepository.Update(survey));

        }
    }
}
