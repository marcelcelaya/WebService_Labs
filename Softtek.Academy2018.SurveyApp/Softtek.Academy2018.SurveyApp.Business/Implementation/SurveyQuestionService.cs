﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Implementation
{
    public class SurveyQuestionService : ISurveyQuestionsService
    {
        private readonly ISurveyRepository _surveyRepository;
        private readonly IQuestionRepository _questionRepository;

        public SurveyQuestionService(ISurveyRepository surveyRepository, IQuestionRepository questionRepository)
        {
            _surveyRepository = surveyRepository;
            _questionRepository = questionRepository;
        }


        public bool AddQuestionToSurvey(int questionId, int surveyId)
        {
            if (questionId <= 0 || surveyId <= 0) return false;
            bool surveyExist = _surveyRepository.Exist(surveyId);
            if (!surveyExist) return false;
            bool questionExist = _questionRepository.Exist(questionId);
            if (!questionExist) return false;

            bool surveyContainQuestion = _surveyRepository.ContainsQuestion(surveyId, questionId);
            if (surveyContainQuestion) return false;
            return _surveyRepository.AddQuestion(surveyId, questionId);
        }

        public ICollection<Question> GetQuestionsBySurvey(int surveyId)
        {
            if (surveyId <= 0) return null;
            return _surveyRepository.GetQuestionsBySurvey(surveyId);
        }
    }
}
